#include "SocketWrapperPCH.hpp"
#include "Server.hpp"

int main()
{
	SocketUtil::StaticInit();
	UDPSocketPtr serverSocket = SocketUtil::CreateUDPSocket(INET);
	SocketAddress serverAddress = SocketAddress(SocketUtil::ConvertIPToInt("127.0.0.1"), 50005);
	serverSocket->Bind(serverAddress);
	serverSocket->SetNonBlockingMode(false);
	DoServiceLoop(serverSocket);
}

void DoServiceLoop(UDPSocketPtr serverSocket)
{
	bool serviceRunning = true;
	char receiveBuffer[MAXPACKETSIZE];
	//the next three lines set the receive buffer to null
	char* begin = receiveBuffer;
	char* end = begin + sizeof(receiveBuffer);
	std::fill(begin, end, 0);

	LinkingContext gameContext;

	SocketAddress senderAddress;
	int bytesReceived = 0;
	int requests=0;

	while (serviceRunning)
	{
		bytesReceived = serverSocket->ReceiveFrom(receiveBuffer, sizeof(receiveBuffer), senderAddress);
		if (bytesReceived > 0)
		{
			requests++;
			ProcessReceivedData(receiveBuffer, bytesReceived, senderAddress, serverSocket, requests, serviceRunning, gameContext);
		}
	}
}

void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress socketAddress, UDPSocketPtr serverSocket, int requests, bool& serviceRunning, LinkingContext& gameContext)
{
	std::cout << "Got " << bytesReceived << " from " << socketAddress.ToString() << std::endl;
	std::cout << "The message is: " << receiveBuffer << std::endl;

	char responseData[MAXPACKETSIZE] = "";
	int choice = atoi(receiveBuffer);
	std::cout << "Choice: " << choice << std::endl;

	std::string currentDateAndTime = "";
	std::string requestString = "";
	std::string quitString = "QUIT";

	switch (choice)
	{
		case static_cast<int>(Choice::ECHO) :
			std::cout << "ECHO request" << std::endl;
			strcpy_s(responseData, receiveBuffer);
			break;

		case static_cast<int>(Choice::DATEANDTIME) :
			std::cout << "DATEANDTIME request" << std::endl;
			currentDateAndTime = ReturnCurrentDateAndTime();
			currentDateAndTime.copy(responseData, currentDateAndTime.length(), 0);
			break;

		case static_cast<int>(Choice::STATS) :
			std::cout << "STATS request" << std::endl;
			requestString = std::to_string(requests);
			requestString.copy(responseData, requestString.length());
			break;

		case static_cast<int>(Choice::QUIT) :
			std::cout << "QUIT request" << std::endl;
			quitString.copy(responseData, quitString.length());
			serviceRunning = false;
			break;

		case static_cast<int>(Choice::NAIVELYSEND) :
			NaiveReceivePlayer(serverSocket);
			std::cout << "NAIVELYSEND request" << std::endl;
			requestString = "Thanks for the player";
			requestString.copy(responseData, requestString.length());
			break;

		case static_cast<int>(Choice::SENDCOMPLEXPLAYER) :
			ReceiveComplexPlayer(serverSocket);
			std::cout << "SENDCOMPLEXPLAYER request" << std::endl;
			requestString = "Thanks for the complex player";
			requestString.copy(responseData, requestString.length());
			break;

		case static_cast<int>(Choice::SENDCOMPRESSEDCOMPLEXPLAYER) :
			ReceiveCompressedComplexPlayer(serverSocket);
			std::cout << "SENDCOMPRESSEDCOMPLEXPLAYER request" << std::endl;
			requestString = "Thanks for the compressed complex player";
			requestString.copy(responseData, requestString.length());
			break;

		case static_cast<int>(Choice::SENDWORLD) :
			ReceiveWorld(serverSocket, gameContext);
			std::cout << "SENDWORLD request" << std::endl;
			requestString = "Thanks for the world";
			requestString.copy(responseData, requestString.length());
			break;
	}
	int byteSent = serverSocket->SendTo(responseData, sizeof(responseData), socketAddress);
}

std::string ReturnCurrentDateAndTime()
{
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);

	std::stringstream ss;
	ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
	return ss.str();
}

void NaiveReceivePlayer(UDPSocketPtr serverSocket)
{
	std::unique_ptr<Player> receiver = std::make_unique<Player>();
	SocketAddress socketAddress;

	int bytesReceived = serverSocket->ReceiveFrom(reinterpret_cast<char*>(receiver.get()), sizeof(Player), socketAddress);
	std::cout << bytesReceived << " from " << socketAddress.ToString() << std::endl;
	receiver->toString();
}

void ReceiveComplexPlayer(UDPSocketPtr serverSocket)
{
	std::unique_ptr<ComplexPlayer> receiver = std::make_unique<ComplexPlayer>();
	SocketAddress senderAddress;

	char* temporaryBuffer = static_cast<char*>(std::malloc(MAXPACKETSIZE));
	int bytesReceived = serverSocket->ReceiveFrom(temporaryBuffer, MAXPACKETSIZE, senderAddress);
	InputMemoryStream stream(temporaryBuffer, static_cast<uint32_t>(bytesReceived));
	receiver->Read(stream);
	
	std::cout << bytesReceived << " from " << senderAddress.ToString() << std::endl;
	receiver->ToString();
}

void ReceiveCompressedComplexPlayer(UDPSocketPtr serverSocket)
{
	std::unique_ptr<ComplexPlayer> receiver = std::make_unique<ComplexPlayer>();
	SocketAddress senderAddress;

	char* temporaryBuffer = static_cast<char*>(std::malloc(MAXPACKETSIZE));
	int bytesReceived = serverSocket->ReceiveFrom(temporaryBuffer, MAXPACKETSIZE, senderAddress);
	InputMemoryBitStream stream(temporaryBuffer, static_cast<uint32_t>(bytesReceived*NUMBER_OF_BITS_IN_A_BYTE));
	receiver->ReadBits(stream);

	std::cout << bytesReceived << " from " << senderAddress.ToString() << std::endl;
	receiver->ToString();
}

void ReceiveWorld(UDPSocketPtr serverSocket, LinkingContext& gameContext)
{
	std::unique_ptr<ComplexPlayer> receiver = std::make_unique<ComplexPlayer>();
	SocketAddress senderAddress;

	char* temporaryBuffer = static_cast<char*>(std::malloc(MAXPACKETSIZE));
	int bytesReceived = serverSocket->ReceiveFrom(temporaryBuffer, MAXPACKETSIZE, senderAddress);
	InputMemoryBitStream stream(temporaryBuffer, static_cast<uint32_t>(bytesReceived * NUMBER_OF_BITS_IN_A_BYTE));
	PacketType receivePacket;
	stream.ReadBits(&receivePacket, 2);
	uint32_t networkId;
	stream.ReadBits(&networkId, 32);
	if (gameContext.GetGameObject(networkId) == nullptr)
	{
		std::cout << "This object does not exist. Need to create it" << std::endl;
		int newId = gameContext.GetNetworkId(receiver.get(), true);
	}
	else
	{
		std::cout << "Object already exists. Can just copy" << std::endl;
	}

	receiver->ReadBits(stream);
	std::cout << bytesReceived << " from " << senderAddress.ToString() << std::endl;
	receiver->ToString();
}



