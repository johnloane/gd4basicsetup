#include "SocketWrapperPCH.hpp"

ComplexPlayer::ComplexPlayer() : mHealth(10), mAmmo(33), mName("Complex Dylan"), mPosition(10,10,10), mRotation(0,0,0,1), mWeapons{60, 70, 80, 90, 100}
{
}

int32_t ComplexPlayer::GetHealth()
{
	return mHealth;
}

int32_t ComplexPlayer::GetAmmo()
{
	return mAmmo;
}

void ComplexPlayer::Write(OutputMemoryStream& outStream) const
{
	outStream.Write(mHealth);
	outStream.Write(mAmmo);
	outStream.Write(mName, 128);
	outStream.Write(mPosition);
	outStream.Write(mRotation);
	outStream.Write(mWeapons);
}

void ComplexPlayer::Read(InputMemoryStream& instream)
{
	instream.Read(mHealth);
	instream.Read(mAmmo);
	instream.Read(mName, 128);
	instream.Read(mPosition);
	instream.Read(mRotation);
	instream.Read(mWeapons);
}

void ComplexPlayer::WriteBits(OutputMemoryBitStream& outBitStream) const
{
	outBitStream.WriteBits(mHealth, 7);
	outBitStream.WriteBits(mAmmo, 7);
	uint8_t nameLength = static_cast<uint8_t>(strlen(mName));
	outBitStream.WriteBits(nameLength, 8);
	outBitStream.WriteBits(mName, (nameLength * 8));
	outBitStream.Write(mWeapons);
	outBitStream.WritePos(mPosition);
	outBitStream.Write(mRotation);
}

void ComplexPlayer::ReadBits(InputMemoryBitStream& inBitStream)
{
	inBitStream.ReadBits(&mHealth, 7);
	inBitStream.ReadBits(&mAmmo, 7);
	uint8_t nameLength;
	inBitStream.Read(nameLength);
	inBitStream.ReadBits(&mName, (nameLength*8));
	inBitStream.Read(mWeapons);
	inBitStream.ReadPos(mPosition);
	inBitStream.Read(mRotation);
}

void ComplexPlayer::ToString() const
{
	std::cout << mName << " has health: " << mHealth << ", ammo: " << mAmmo << " Position: (" << mPosition.mX << "," << mPosition.mY << "," << mPosition.mZ << ")" << " Quaternion: (" << mRotation.mX << "," << mRotation.mY << "," << mRotation.mZ << "," << mRotation.mW << ")" << std::endl;
	for (int i = 0; i < mWeapons.size(); ++i)
	{
		std::cout << mWeapons[i] << ", ";
	}
	std::cout << std::endl;
}
