#pragma once
void DoServiceLoop(UDPSocketPtr serverSocket);
void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress socketAddress, UDPSocketPtr serverSocket, int requests, bool& serviceRunning, LinkingContext& gameContext);
std::string ReturnCurrentDateAndTime();
void NaiveReceivePlayer(UDPSocketPtr serverSocket);
void ReceiveComplexPlayer(UDPSocketPtr serverSocket);
void ReceiveCompressedComplexPlayer(UDPSocketPtr serverSocket);
void ReceiveWorld(UDPSocketPtr serverSocket, LinkingContext& gameContext);
enum class Choice { ECHO = 1, DATEANDTIME = 2, STATS = 3, NAIVELYSEND = 4, SENDCOMPLEXPLAYER = 5, SENDCOMPRESSEDCOMPLEXPLAYER = 6, SENDWORLD=7, QUIT=8 };
