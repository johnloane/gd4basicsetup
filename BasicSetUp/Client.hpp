#pragma once
void DoServiceLoop(UDPSocketPtr clientSocket);
void PrintOptions();
void GetChoice(string& choice);
void SendDataToServer(UDPSocketPtr clientSocket, char* input);
void ReceiveDataFromServer(UDPSocketPtr clientSocket, char* receiveBuffer, SocketAddress senderAddress, int bytesReceived, bool& serviceRunning);
void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress senderAddress, bool& serviceRunning);
void NaivelySendPlayer(UDPSocketPtr clientSocket, const Player* player);
void SendComplexPlayer(UDPSocketPtr clientSocket, const ComplexPlayer* player);
void SendCompressedComplexPlayer(UDPSocketPtr clientSocket, const ComplexPlayer* player);
void SendWorld(UDPSocketPtr clientSocket, ComplexPlayer* player, LinkingContext gameContext);
const int32_t MAXPACKETSIZE = 1300;