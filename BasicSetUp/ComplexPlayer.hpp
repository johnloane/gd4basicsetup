class ComplexPlayer : public GameObject
{
public:
	ComplexPlayer();
	int32_t GetHealth();
	int32_t GetAmmo();

	void Write(OutputMemoryStream& outStream) const;
	void Read(InputMemoryStream& instream);

	void WriteBits(OutputMemoryBitStream& outBitStream) const;
	void ReadBits(InputMemoryBitStream& inBitStream);

	void ToString() const;
private:
	int32_t mHealth;
	int32_t mAmmo;
	char mName[128];
	Vector3 mPosition;
	Quaternion mRotation;
	vector<int32_t> mWeapons;
};