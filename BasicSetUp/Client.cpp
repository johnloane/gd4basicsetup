#include "SocketWrapperPCH.hpp"
#include "Client.hpp"


int main()
{
	std::cout << sizeof(Player) << std::endl;
	SocketUtil::StaticInit();
	UDPSocketPtr clientSocket = SocketUtil::CreateUDPSocket(INET);
	clientSocket->SetNonBlockingMode(false);
	DoServiceLoop(clientSocket);
}

void DoServiceLoop(UDPSocketPtr clientSocket)
{
	bool serviceRunning = true;
	string choice;
	char receiveBuffer[MAXPACKETSIZE];
	//the next three lines set the receive buffer to null
	char* begin = receiveBuffer;
	char* end = begin + sizeof(receiveBuffer);
	std::fill(begin, end, 0);

	LinkingContext gameContext;

	unique_ptr<Player> joshua = std::make_unique<Player>();

	unique_ptr<ComplexPlayer> dylan = std::make_unique<ComplexPlayer>();


	SocketAddress senderAddress;
	int bytesReceived = 0;

	while (serviceRunning)
	{
		PrintOptions();
		GetChoice(choice);
		SendDataToServer(clientSocket, (char*)choice.c_str());
		if (std::stoi(choice) == 4)
		{
			NaivelySendPlayer(clientSocket, joshua.get());
		}
		else if (std::stoi(choice) == 5)
		{
			SendComplexPlayer(clientSocket, dylan.get());
		}
		else if (std::stoi(choice) == 6)
		{
			SendCompressedComplexPlayer(clientSocket, dylan.get());
		}
		else if (std::stoi(choice) == 7)
		{
			SendWorld(clientSocket, dylan.get(), gameContext);
		}
		
		ReceiveDataFromServer(clientSocket, receiveBuffer, senderAddress, bytesReceived, serviceRunning);
	}
}

void PrintOptions()
{
	std::cout << "Please enter: " << std::endl;
	std::cout << "1) To use the ECHO service: " << std::endl;
	std::cout << "2) To use the DATEANDTIME service: " << std::endl;
	std::cout << "3) To use the STATS service: " << std::endl;
	std::cout << "4) To Naively send a player: " << std::endl;
	std::cout << "5) To send the complex player using byte stream: " << std::endl;
	std::cout << "6) To send the compressed complex player: " << std::endl;
	std::cout << "7) To send the world using object replication: " << std::endl;
	std::cout << "8) To QUIT the service: " << std::endl;
}

void GetChoice(string& choice)
{
	std::getline(std::cin, choice);
}

void SendDataToServer(UDPSocketPtr clientSocket, char* input)
{
	std::cout << SocketUtil::ConvertIPToInt("127.0.0.1") << std::endl;
	SocketAddress serverAddress = SocketAddress(SocketUtil::ConvertIPToInt("127.0.0.1"), 50005);
	int bytesSent = clientSocket->SendTo(input, strlen(input), serverAddress);
}


void ReceiveDataFromServer(UDPSocketPtr clientSocket, char* receiveBuffer, SocketAddress senderAddress, int bytesReceived, bool& serviceRunning)
{
	bytesReceived = clientSocket->ReceiveFrom(receiveBuffer, MAXPACKETSIZE, senderAddress);
	if (bytesReceived > 0)
	{
		ProcessReceivedData(receiveBuffer, bytesReceived, senderAddress, serviceRunning);
	}
}

void ProcessReceivedData(char* receiveBuffer, int bytesReceived, SocketAddress senderAddress, bool& serviceRunning)
{
	if (strcmp("QUIT", receiveBuffer) == 0)
	{
		std::cout << "Server says we have to shut down..." << std::endl;
		serviceRunning = false;
	}
	std::cout << "Receive Buffer size " << strlen(receiveBuffer) << std::endl;
	std::cout << "Got " << bytesReceived << " from " << senderAddress.ToString() << std::endl;
	std::cout << "The message is: " << receiveBuffer << std::endl;
}

void NaivelySendPlayer(UDPSocketPtr clientSocket, const Player* player)
{
	std::cout << "Player has size: " << sizeof(Player) << std::endl;
	SocketAddress serverAddress = SocketAddress(SocketUtil::ConvertIPToInt("127.0.0.1"), 50005);
	int bytesSent = clientSocket->SendTo(reinterpret_cast<const char*>(player), sizeof(Player), serverAddress);
	std::cout << "Sent player of size " << sizeof(Player) << "to the server" << std::endl;
}

void SendComplexPlayer(UDPSocketPtr clientSocket, const ComplexPlayer* player)
{
	SocketAddress serverAddress = SocketAddress(SocketUtil::ConvertIPToInt("127.0.0.1"), 50005);
	OutputMemoryStream outStream;
	player->Write(outStream);
	int bytesSent = clientSocket->SendTo(outStream.GetBufferPtr(), outStream.GetLength(), serverAddress);
	std::cout << "Sent: " << bytesSent << std::endl;
}

void SendCompressedComplexPlayer(UDPSocketPtr clientSocket, const ComplexPlayer* player)
{
	SocketAddress serverAddress = SocketAddress(SocketUtil::ConvertIPToInt("127.0.0.1"), 50005);
	OutputMemoryBitStream outBitStream;
	player->WriteBits(outBitStream);
	int bytesSent = clientSocket->SendTo(outBitStream.GetBufferPtr(), outBitStream.GetByteLength(), serverAddress);
	std::cout << "Sent: " << bytesSent << std::endl;
}

void SendWorld(UDPSocketPtr clientSocket, ComplexPlayer* player, LinkingContext gameContext)
{
	SocketAddress serverAddress = SocketAddress(SocketUtil::ConvertIPToInt("127.0.0.1"), 50005);
	OutputMemoryBitStream outBitStream;
	//First sent the type of packet
	PacketType orPacket = PacketType::PT_ReplicationData;
	outBitStream.WriteBits(&orPacket, 2);
	//Second send the id of the object
	uint32_t networkId = gameContext.GetNetworkId(player, true);
	std::cout << "Id is: " << networkId << std::endl;
	outBitStream.WriteBits(networkId, sizeof(uint32_t)* NUMBER_OF_BITS_IN_A_BYTE);
	player->WriteBits(outBitStream);
	int bytesSent = clientSocket->SendTo(outBitStream.GetBufferPtr(), outBitStream.GetByteLength(), serverAddress);
	std::cout << "Sent: " << bytesSent << std::endl;
}
