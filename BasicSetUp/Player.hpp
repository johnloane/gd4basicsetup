#pragma once
class Player
{
public:
	Player() : mHealth(10), mAmmo(3) {}
	Player(int Health, int Ammo) : mHealth(Health), mAmmo(Ammo) {}
	int getHealth() const;
	int getAmmo() const;
	void toString() const;

private:
	int32_t mHealth;
	int32_t mAmmo;
};