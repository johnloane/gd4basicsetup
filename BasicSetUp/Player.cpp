#include "SocketWrapperPCH.hpp"

int Player::getHealth() const
{
	return mHealth;
}

int Player::getAmmo() const
{
	return mAmmo;
}

void Player::toString() const
{
	std::cout << "Health: " << mHealth << ",Ammo: " << mAmmo << std::endl;
}
